﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string path;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)  //the file is being selected...
        {
            if (openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                path = openFileDialog1.FileName;
                string file_name = Path.GetFileName(path);
                textBox1.Text = @"...\"+file_name;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            bool accepted = true;   //Have been recognized...
            Stack<char> parentheses = new Stack<char>();    //stack of NAS
            string remaining_parentheses;   //used to count all the parentheses
            string step;    //used to describe the step that NAS did
            richTextBox1.Clear();
            try
            {
                if (textBox1.TextLength != 0)
                {
                    using(StreamReader sr = new StreamReader(path))
                    {
                        remaining_parentheses = parentheses_counter(path);
                        int cnt = remaining_parentheses.Length / 2; //.../2: the parenteses without spaces
                        richTextBox1.Text = "The input file consists of this parentheses' sequence: " + remaining_parentheses + "   <" + cnt + ">" + Environment.NewLine + Environment.NewLine;
                        while (sr.Peek() >= 0)
                        {
                            char ch = (char)sr.Read();
                            if (Convert.ToInt32(ch) == 40)  //checks if the character is "("
                            {
                                parentheses.Push(ch);   //add the "(" in the stack
                                //remaining_parentheses += ch + " ";
                                if (checkBox1.Checked)
                                {   //if selected by the user, prints the step that the NAS did and the stack after the change
                                    step = "~Found ' ( ', Push( ' ( ' ) into the Stack" + Environment.NewLine + "Stack Status: " + stack_printer(parentheses) + Environment.NewLine + Environment.NewLine;
                                    richTextBox1.Text += step;
                                }
                            }
                            else if (Convert.ToInt32(ch) == 41) //checks if the character is ")"
                            {
                                //remaining_parentheses += ch + " ";
                                if (parentheses.Count == 0)
                                {   //the entry has not been recognized, because although the stack was empty the program read ")"
                                    accepted = false;
                                    if (checkBox1.Checked)
                                    {   //if selected by the user, prints the reason for which the entry wasn't recognized 
                                        step = "~Found ' ) '. Although the Stack is empty the next character is ' ) ' so the input could not be recognized..." + Environment.NewLine + "Stack Status: " + stack_printer(parentheses) + Environment.NewLine + Environment.NewLine;
                                        richTextBox1.Text += step;
                                    }
                                    break;
                                }
                                else
                                {
                                    parentheses.Pop();
                                    if (checkBox1.Checked)
                                    {   //if selected by the user, prints the step that the NAS did and the stack after the change
                                        step = "~Found ' ) ', Pop( )" + Environment.NewLine + "Stack Status: " + stack_printer(parentheses) + Environment.NewLine + Environment.NewLine;
                                        richTextBox1.Text += step;
                                    }
                                }
                            }                            
                        }
                    }
                    if (parentheses.Count != 0)
                        accepted = false;   //this means that "(" are more than ")"
                    richTextBox1.Text += "--------------------------------------------------------------------------------" + Environment.NewLine/* + remaining_parentheses + Environment.NewLine +*/;
                    if (checkBox1.Checked)  //if selected by the user, prints the stack status
                        richTextBox1.Text += "Stack Status: " + stack_printer(parentheses) + Environment.NewLine;
                    richTextBox1.Text += "Have been recognized: " + accepted.ToString();                  
                }
                else
                    MessageBox.Show("Choose a file first!");    //prevents the user from not selecting file
            }
            catch(FileNotFoundException e1)
            {
                MessageBox.Show(e1.Message);
            }
            catch(InvalidOperationException e2)
            {
                MessageBox.Show(e2.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)  //refresh Button
        {
            reload();
        }

        private string parentheses_counter(string p)    //reads the whole file and put all the parantheses (lefts n rights) and puts them in a string with spaces in between
        {
            using (StreamReader sr = new StreamReader(p))
            {
                string parentheses = "";
                while (sr.Peek() >= 0)
                {
                    char ch = (char)sr.Read();
                    if (Convert.ToInt32(ch) == 40)  //if "("
                    {
                        parentheses += ch + " ";
                    }
                    else if (Convert.ToInt32(ch) == 41) //if ")"
                    {
                        parentheses += ch + " ";
                    }
                }
                return parentheses;
            }
        }

        private string stack_printer(Stack<char> stk)   //returns, in a string, the content (char) of a stack (string), in case of empty stack returns "(empty)"
        {
            string stk_elements="";
            if (stk.Count != 0)
            {
                foreach (char x in stk)
                {
                    stk_elements = x + " " + stk_elements;
                }
            }
            else
                stk_elements = "(empty)";
            return stk_elements;
        }

        private void reload()   //required actions, so the user can re-use the application
        {
            textBox1.Clear();
            richTextBox1.Clear();
            checkBox1.Checked = false;
            openFileDialog1.Reset();
        }
    }
}
