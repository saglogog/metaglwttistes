#include <string>
#include <random>
#include <iostream>

using namespace std;


// create engine for random number generation
mt19937 return_engine(){
  random_device r;
  return mt19937(r());
}

// class that contains the methods returning the random choices
struct Randomizer {
  mt19937 e1 = return_engine();   
  
  Randomizer() = default;
  
  int miz1(){
    uniform_int_distribution<> dis (0, 1);
    return dis(e1);
  };
  int miz2(){
    uniform_int_distribution<> dis2 (0, 2);
    return dis2(e1);
  };
} rm;


// function representing the factor grammar rule
string factor(){
  int choice = rm.miz2();

  if (choice==0) {
    cout << "<factor> ::= a\n";
    return "a";
  }
  else if (choice==1) {
    cout << "<factor> ::= b\n";
    return "b";
  }
  else {
    cout << "<factor> ::= c\n";
    return "c";
  }
}

// function representing the term grammar rule
string term(){
  int choice = rm.miz1();
  
  if (choice==0) {  
    cout << "<term> ::= <factor>\n";
    return factor();
  }
  else {
    cout << "<term> ::= <term> * <factor>\n";
    return term() + "*" + factor();
  }
}

// function representing the expression grammar rule
string expression(){
  int choice = rm.miz1();
  
  if (choice==0)  {
    cout << "<expression> ::= <term>\n";
    return term();
  }
  else {
    cout << "<expression> ::= <expression> + <term>\n";
    return expression() + "+" + term();
  }
}

int main(){
  string final_product = expression();
  cout << "The string that was produced is: " << final_product << "\n";
  return 0;
}

