// ll(1) grammar parser implementation
#include <iostream>
#include <stack>
#include <string>
#include <algorithm>

using namespace std;

// function that emulates the ppt
// maps the non-terminal -terminal combinations to the correct grammar production
// throws exceptions when incorrect symbols are entered
string return_correct_symbol(char stack_symbol, char lookahead_symbol){
  string correct_symbol;
  
  switch(stack_symbol){
    case 'S':
      if(lookahead_symbol == '[') correct_symbol = "[A]";
      else throw 1;
      break;
    case 'A':
      if(lookahead_symbol == 'x' || lookahead_symbol == 'y' || lookahead_symbol == '[') correct_symbol = "BE";
      else throw 1;
      break;
    case 'B':
      if(lookahead_symbol == 'x') correct_symbol = "x";
      else if (lookahead_symbol == 'y') correct_symbol = "y";
      else if (lookahead_symbol == '[') correct_symbol = "S";
      else throw 1;
      break;
    case 'E':
      if(lookahead_symbol == ':') correct_symbol = ":A";
      else if (lookahead_symbol == '+') correct_symbol = "+A";
      else if (lookahead_symbol == ']') correct_symbol = "e";
      else throw 1;
      break;
    default:
      throw 2;
      break;
  }

  return correct_symbol;
}

// the parser is implemented inside the main function
int main(){
  // get string
  string input;
  cout << "Enter the string to be recognized: \n";
  cin >> input;
  
  // get first lookahead symbol 
  char input_char = input[0];
  
  // will contain the result of xthe production
  string correct_symbol;
  
  // declare & initialize stack
  stack<char> symbols_stack;
  symbols_stack.push('$');
  symbols_stack.push('S');
   
  // initialize stack top
  char cur_stack_top = symbols_stack.top();

  // where the magic happens
  try{
    // while there are still symbols to be recognized
    while (cur_stack_top != '$'){
      // if stack top matches the next lookup (a) pop the stack and go to the next symbol
      if(cur_stack_top == input_char){
        symbols_stack.pop();
        input.erase(input.begin());
        input_char = input[0];
      }
      // if X (stack top) is non-terminal the string doesn't belong to the grammar
      else if(cur_stack_top == 'x' || cur_stack_top == 'y' || cur_stack_top == ':' || cur_stack_top == '+' || cur_stack_top == '[' || cur_stack_top == ']') throw 3;
      // simply pop the stack if we have ε
      else if(cur_stack_top == 'e') symbols_stack.pop();
      // get the next production if it exists, else the string is unrecognizable
      else {
        correct_symbol = return_correct_symbol(cur_stack_top, input_char);
	cout << "Using the production: " << cur_stack_top << "-->" << correct_symbol << "\n";
	symbols_stack.pop();
        // push the production symbols in the stack reversed
	reverse(correct_symbol.begin(), correct_symbol.end());
	for(int i = 0; i < correct_symbol.length(); i++){
          symbols_stack.push(correct_symbol[i]);
	}
      }
      // the new stack top
      cur_stack_top = symbols_stack.top();
    }
  }
  catch(int n){
    if (n == 1) cout << "An entry M[X,a] occured that doesn't belong in the ppt (wrong a = " << input_char << ").\n";
    else if(n == 2) cout << "An entry M[X,a] occured that doesn't belong in the ppt (wrong X = " << cur_stack_top << ").\n";
    else if(n == 3) cout << "Unrecognized a entered. The input string could not be recognized (a = " << input_char << ").\n";
  }
  
  cout << "String recognized!\n";
  return 0;
}

