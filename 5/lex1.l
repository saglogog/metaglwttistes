letter [A-E]
%%
Given|given {printf("\n%s : it's a verb", yytext);}
angle|triangle|square {printf("\n%s : it's a geometric entity", yytext);}
{letter}+ {printf("\n%s : it's the name of the geometric entity", yytext);}
[ \t\n]+
. {printf("\n%s: Unknown entity", yytext);}
%%
main()
{
  printf("\n(ex. Given triangle BCD)\n\n");
  yylex();
}
